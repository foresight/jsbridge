package
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.NetStatusEvent;
	import flash.events.StatusEvent;
	import flash.events.ActivityEvent;
	import flash.external.ExternalInterface;
	import flash.media.Camera;
	import flash.media.H264Level;
	import flash.media.H264Profile;
	import flash.media.H264VideoStreamSettings;
	import flash.media.Microphone;
	import flash.media.SoundCodec;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.Responder;
	import flash.utils.ByteArray;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;

	public class foresight extends Sprite
	{
		private var nc:NetConnection;
		private var loglevel:int;

		// stuff for buffering JS calls since EI sucks
		private var eiarray:Array;
		private var last_eitime:Number;
		private var ei_interval:Number;

		private var videos:Array;
		private var streams:Array;
		private var cameras:Array;
		private var mics:Array;
		public function foresight()
		{
			if (!ExternalInterface.available) {
				trace("ExternalInterface not available");
				return;
			}
			ExternalInterface.addCallback("foresight_new", nc_new);
			ExternalInterface.addCallback("foresight_connect", connect);
			ExternalInterface.addCallback("foresight_play", play);
			ExternalInterface.addCallback("foresight_publish", publish);
			ExternalInterface.addCallback("foresight_loglevel", set_level);
			ExternalInterface.addCallback("foresight_video", set_video);
			ExternalInterface.addCallback("foresight_camera", set_camera);
			ExternalInterface.addCallback("foresight_mic", set_mic);
			ExternalInterface.addCallback("foresight_cameras", get_cameras);
			ExternalInterface.addCallback("foresight_mics", get_mics);
			ExternalInterface.addCallback("foresight_call", invoke_call);
			ExternalInterface.addCallback("foresight_frame", get_frame);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			nc = null;
			loglevel = 0;
			eiarray = new Array();
			last_eitime = 0;
			videos = new Array();
			streams = new Array();
			cameras = new Array();
			mics = new Array();
			INFO("JS Bridge Ready");
			ExternalInterface.call("foresight_ready");
		}
		private function nc_new(client:Object):void
		{
			if (nc) return;
			nc = new NetConnection();
			nc.objectEncoding = flash.net.ObjectEncoding.AMF0;
			var cl:Object = {};
			for (var key:String in client) {
				cl[key] = set_cb(client[key]);
			}
			nc.addEventListener(NetStatusEvent.NET_STATUS, function nsstatus(ev:NetStatusEvent):void {
				INFO("NetStatusEvent: "+ev.info.code);
				if ("onStatus" in nc.client) nc.client["onStatus"](ev.info);
			});
			nc.client = cl;
		}
		private function connect(uri:String, ...parms):int
		{
			if (!nc || nc.connected) {
				ERR("Bad NetConnection");
				return -1;
			}
			parms.unshift(uri);
			nc.connect.apply(nc, parms);
			INFO("Connecting to "+uri);
			return 0;
		}
		private function play(props:Object):int
		{
			return setup_nsstream(props, false);
		}
		private function publish(props:Object):int
		{
			return setup_nsstream(props, true);
		}
		private function set_video(props:Object):int
		{
			var v:Video, ns:NetStream = null;
			if ("remove" in props) {
				v = videos[props.remove];
				if (null == v || !contains(v)) {
					ERR("Unable to remove video "+props.remove);
					return -1;
				}
				INFO("Removing video "+props.remove);
				removeChild(v);
				v.attachCamera(null);
				delete videos[props.remove];
				return props.remove;
			}
			if ("id" in props) v = videos[props.id];
			else v = new Video();
			if (null == v) {
				ERR("Could not get video");
				return -1;
			}
			if ("stream" in props) {
				ns = streams[props.stream];
				if (null == ns) {
					ERR("Could not get NetStream "+props.stream);
					return -1;
				}
				v.attachNetStream(ns);
			}
			if ("camera" in props) {
				var c:Camera = cameras[props.camera];
				if (null == c) {
					ERR("Could not get camera");
					return -1;
				}
				v.attachCamera(c);
			}
			if ("width"  in props) v.width = props.width;
			if ("height" in props) v.height = props.height;
			if ("x" in props) v.x = props.x;
			if ("y" in props) v.y = props.y;
			if ("vol" in props || "pan" in props) {
				if (null == ns) {
					ERR("Select NetStream to set audio properties");
					return -1;
				}
				var vol:Number = ns.soundTransform.volume;
				var pan:Number = ns.soundTransform.pan;
				if ("vol" in props) vol = props.vol;
				if ("pan" in props) pan = props.pan;
				ns.soundTransform = new SoundTransform(vol, pan);
			}
			if (!contains(v)) addChild(v);
			var vid:int = "id" in props ? props.id : videos.push(v) - 1;
			INFO("Setting properties for video "+vid);
			return vid;
		}
		private function set_camera(props:Object):int
		{
			var c:Camera, bw:int, qual:int, w:int, h:int, fps:int;
			var h264:H264VideoStreamSettings = null;
			if ("close" in props) {
				c = cameras[props.close];
				if (null == c) {
					ERR("Unable to close camera "+props.close);
					return -1;
				}
				cameras[props.close] = null;
				delete cameras[props.close];
				c = null;
				INFO("Closing camera "+props.close);
				return props.close;
			}
			if ("id" in props) c = cameras[props.id];
			else if ("name" in props) c = Camera.getCamera(props.name);
			else c = Camera.getCamera();
			if (null == c) {
				ERR("Unable to get camera");
				return -1;
			}
			w = "width" in props ? props.width : 160;
			h = "height" in props ? props.height : 120;
			fps = "fps" in props ? props.fps : 15;
			bw = "bandwidth" in props ? props.bandwidth : 0;
			qual = "quality" in props ? props.quality : 0;
			if ("h264" in props) {
				var p:Object = props.h264;
				var level:String = H264Level.LEVEL_1;
				var profile:String = H264Profile.BASELINE;
				h264 = new H264VideoStreamSettings();
				if (null != p && "profile" in p) profile = p.profile;
				if (null != p && "level" in p) level = p.level;
				h264.setProfileLevel(profile, level);
				INFO("Setting H264 profile: "+profile+" level: "+level);
			}
			if ("stream" in props) {
				var ns:NetStream = streams[props.stream];
				if (null == ns) {
					ERR("Unable to get NetStream "+props.stream);
					return -1;
				}
				ns.attachCamera(c);
				if (null != h264) ns.videoStreamSettings = h264;
			}
			if ("keyint" in props) c.setKeyFrameInterval(props.keyint);
			c.setMode(w, h, fps);
			c.setQuality(bw, qual);
			var cid:int = "id" in props ? props.id : cameras.push(c) - 1;
			INFO("Setting properties for camera "+cid);
			return cid;
		}
		private function set_mic(props:Object):int
		{
			var m:Microphone;
			if ("close" in props) {
				m = mics[props.close];
				if (null == m) {
					ERR("Unable to close microphone "+props.close);
					return -1;
				}
				mics[props.close] = null;
				delete mics[props.close];
				m = null;
				INFO("Closing microphone "+props.close);
				return props.close;
			}
			if ("id" in props) m = mics[props.id];
			else if ("name" in props) m = Microphone.getMicrophone(props.name);
			else m = Microphone.getMicrophone();
			if (null == m) {
				ERR("Unable to get microphone");
				return -1;
			}
			if ("codec" in props) m.codec = props.codec;
			if ("gain" in props) m.gain = props.gain;
			if ("rate" in props) m.rate = props.rate;
			if ("speex" in props && null != props.speex) {
				var p:Object = props.speex;
				m.codec = SoundCodec.SPEEX;
				if ("noiseSuppressionLevel" in p) m.noiseSuppressionLevel = p.noiseSuppressionLevel;
				if ("encodeQuality" in p) m.encodeQuality = p.encodeQuality;
				if ("framesPerPacket" in p) m.framesPerPacket = p.framesPerPacket;
				if ("enableVAD" in p) m.enableVAD = "true" == p.enableVAD;
			}
			if ("loopback" in props) m.setLoopBack(props.loopback);
			if ("silenceLevel" in props || "silenceTimeout" in props) {
				var silenceLevel:Number = m.silenceLevel;
				var timeout:int = m.silenceTimeout;
				if ("silenceLevel" in props) silenceLevel = props.silenceLevel;
				if ("silenceTimeout" in props) timeout = props.silenceTimeout;
				m.setSilenceLevel(silenceLevel, timeout);
			}
			if ("vol" in props || "pan" in props) {
				var vol:Number = m.soundTransform.volume;
				var pan:Number = m.soundTransform.pan;
				if ("vol" in props) vol = props.vol;
				if ("pan" in props) pan = props.pan;
				m.soundTransform = new SoundTransform(vol, pan);
			}
			if ("stream" in props) {
				var ns:NetStream = streams[props.stream];
				if (null == ns) {
					ERR("Unable to get NetStream "+props.stream);
					return -1;
				}
				ns.attachAudio(m);
			}
			var mid:int = "id" in props ? props.id : mics.push(m) - 1;
			if ("status" in props) {
				m.addEventListener(StatusEvent.STATUS, set_cb(props.status));
			}
			if ("activity" in props) {
				m.addEventListener(ActivityEvent.ACTIVITY, set_cb(props.activity));
			}
			INFO("Setting properties for mic "+mid);
			return mid;
		}
		private function get_cameras():Array
		{
			INFO(Camera.names.length+" cameras installed");
			return Camera.names;
		}
		private function get_mics():Array
		{
			INFO(Microphone.names.length+" microphones installed");
			return Microphone.names;
		}
		private function invoke_call(method:String, res:String, err:String, ...parms):int
		{
			if (null == method) {
					ERR("Unable to get method for invoke");
					return -1;
			}
			var resp:Responder = null, res_cb:Function = null, err_cb:Function = null
			if (res != null) res_cb = set_cb(res);
			if (err != null) err_cb = set_cb(err);
			if (res != null || err != null) resp = new Responder(res_cb, err_cb);
			parms.unshift(resp);
			parms.unshift(method);
			nc.call.apply(nc, parms);
			INFO("Calling method "+method);
			return 0;
		}
		private function get_frame(id:int):String
		{
			var v:Video = videos[id], bd:BitmapData;
			if (null == v) {
				ERR("Invalid video id");
				return null;
			}
			if (0 == v.videoWidth || 0 == v.videoHeight) {
				ERR("Video has invalid dimensions; couldn't capture");
				return null;
			}
			bd = new BitmapData(v.videoWidth, v.videoHeight);
			bd.draw(v);
			return Base64.encodeByteArray(PNGEncoder.encode(bd));
		}
		private function setup_nsstream(props:Object, isPublish:Boolean):int
		{
			var ns:NetStream;
			if ("close" in props) {
				ns = streams[props.close];
				if (null == ns) {
					ERR("Unable to find NetStream "+props.close);
					return -1;
				}
				INFO("Closing NetStream "+props.close);
				if (isPublish) {
					ns.attachCamera(null);
					ns.attachAudio(null);
				}
				ns.close();
				return props.close;
			}
			var name:String = props.name;
			if (null == name) {
				ERR("Missing required field \"name\" in props");
				return -1;
			}
			if (!nc || !nc.connected) {
				ERR("Bad NetConnection");
				return -1;
			}
			if ("id" in props) ns = streams[props.id];
			else ns = new NetStream(nc);
			if (null == ns) {
				ERR("Unable to find NetStream "+props.id);
				return -1;
			}
			var callback:String = props.callback;
			var si:int = "id" in props ? props.id : streams.push(ns) -1;
			const STREAMSTATUS:String = "__streamStatus__";
			if (null != callback) {
				nc.client[STREAMSTATUS+si] = set_cb(callback);
				ns.addEventListener(NetStatusEvent.NET_STATUS, function(ev:NetStatusEvent):void {
					INFO("NetStream event for "+si+" : "+ev.info.code);
					ev.info.target = si;
					nc.client[STREAMSTATUS+si](ev.info);
				});
			}
			if (isPublish) {
				if ("video" in props) {
					var v:Video = videos[props.video];
					if (null == v) {
						ERR("Could not get video");
						return -1;
					}
				    v.attachNetStream(ns);
				}
				if ("mic" in props) {
					var m:Microphone = mics[props.mic];
					if (null == m) {
						ERR("Could not get microphone");
						return -1;
					}
					ns.attachAudio(m);
				}
			}
			INFO("Streaming "+name);
			isPublish ? ns.publish(name, "live") : ns.play(name);
			return si;
		}
		private function set_cb(val:String):Function {
			return function(...parms):void {
				eiarray.push(function():void{
					INFO("Calling "+val);
					try{
						if (1 == parms.length) ExternalInterface.call(val, parms[0]);
						else ExternalInterface.call(val, parms);
					} catch(error:Error) {
						ERR("Error occurred: "+error.message);
					}
				});
				call_js();
			}
		}
		private const EI_DELAY:int = 10;
		private function call_js():void
		{
			var now:Number = (new Date()).valueOf();
			if (now - last_eitime > EI_DELAY) {
				DEBUG("call_js popping "+eiarray.length+" isnan? "+isNaN(ei_interval));
				eiarray.shift()();
				last_eitime = now;
				if (eiarray.length == 0 && !isNaN(ei_interval)){
					DEBUG("Clearing EI interval "+ei_interval);
					clearInterval(ei_interval);
					ei_interval = NaN;
				}
			} else {
				DEBUG("call_js retaining");
				if (isNaN(ei_interval)) {
					ei_interval = setInterval(call_js, EI_DELAY);
					DEBUG("Setting EI interval "+ei_interval);
				}
			}
		}
		private function set_level(level:int):void
		{
			loglevel = level;
		}
		private function ERR(str:String):void { log(0, str); }
		private function INFO(str:String):void { log(1, str); }
		private function DEBUG(str:String):void { log(2, str); }
		private function log(level:int, str:String):void
		{
			if (loglevel < level) return;
			var s:String = "Foresight: "+str;
			trace(s);
			ExternalInterface.call("console.log", s);
		}
	}
}
