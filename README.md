# Foresight AS3-JS Bridge
This is a thin wrapper around Flash's NetConnection/NetStream (RTMP) functions allowing manipulation from JS.

1. [Prerequisites](#prerequisites)
2. [Building](#building)
3. [Usage](#usage)
    * [foresight\_new](#foresight_new)
    * [foresight\_connect](#foresight_connect)
    * [foresight\_play](#foresight_play)
    * [foresight\_publish](#foresight_publish)
    * [foresight\_call](#foresight_call)
    * [foresight\_video](#foresight_video)
    * [foresight\_camera](#foresight_camera)
    * [foresight\_mic](#foresight_mic)
    * [foresight\_cameras](#foresight_cameras)
    * [foresight\_mics](#foresight_mics)
    * [foresight\_frame](#foresight_frame)
    * [foresight_loglevel](#foresight_loglevel)
    * [Example](#example)
4. [Notes](#notes)

## Prerequisites:
Flex SDK. Only tested with 4.6, should work on most earlier versions.

## Building:

### SDK
    mxmlc foresight.as

Assumes the Flex SDK is installed and mxmlc is within the PATH.

### Flash Builder
Drop `foresight.as` into a Flash Builder project.

## Usage
When the SWF is ready for use, this is called:

    foresight_ready()
All further operations on the swf should be done within or after this function.

The following functions are exported to JS:

### foresight_new
    foresight_new(callbacks:Object)
Initializes the NetConnection with the given callbacks. The callback object is of the form `{<invoke_name> : <js_function>}` where the `invoke_name` is the name of an RTMP invoke you expect, and `js_function` is the (string) name of the JS function to call. The actual function `js_function` expects a signature of the form:

> `function js_function(args:Array)`

where `args[0]` represents the first parameter, `args[1]` the second, and so on.

If only one parameter is passed, `args` takes the type of that parameter, rather than being an array, eg `function js_function(args:String)` for a single string.

The `onStatus` name is special-cased to also respond to NetConnection and NetStream status events; this can be wired into a JS function of your choice.

### foresight_connect
    foresight_connect(uri:String, parameters:...)
Connects to the given uri with the given parameters. The parameters can be composed of JSON literals. Multiple parameters (delimited by a comma) are passed as an array.

### foresight_play
    foresight_play(props:Object)
Plays the given stream. Returns a stream ID, or < 0 for error. Valid properties are:

* `name` Name of the stream to play. Required.
* `callback` : String indicating the Javascript function to call for NetStream events.
* `close` : ID of a stream to close, given by a previous call to `foresight_play`.

The `callback` function will be called with an array containing a single object. Some relevant properties of the object are:
* `target` id of the NetStream, as given by foresight_publish or foresight_play.
* `code` Message code, such as NetStream.Play.Start or NetStream.Play.Stop

Callback parameters can be inspected by calling `console.log(param);`.

### foresight_publish
    foresight_publish(props:Object)
Publishes the given stream. Returns a stream ID, or < 0 for error. For a description of valid properties, see [foresight\_play](#foresight_play). In addition, the following properties are accepted:

* `video` Video ID as returned by `foresight_video` to attach to the NetStream.
* `mic` Microphone ID as returned by `foresight_mic` to attach to the NetStream.

### foresight_call
    foresight_call(method:String, res:String, err:String, parameters:...)
Sends out a custom RTMP message.

* `method` : Name of the message (required).
* `res` : String indicating the Javascript function to call upon a successful response. Can be null.
* `error` : String indicating the Javascript function to call in case of an error. Can be null.
* `parameters` : Any additonal parameters to pass to the call, in the form of JSON literals.

### foresight_video
    foresight_video(opts:Object)
Manipulate video objects. Returns a video ID, or < 0 for error. Valid options are:

* `id` : Id of an existing video from a previous call to foresight\_video. If not supplied, a new video object is created.
* `remove` : Id of the video to remove from the stage, as given by a previous call to `foresight\_video`.
* `stream` : Id of the stream to attach to, as given by foresight_play.
* `camera` : Id of the camera to attach to, as given by foresight_camera.
* `width` : Width of the video.
* `height` : Height of the video.
* `x` : X-coordinate within the swf.
* `y` : Y-coordinate within the swf.
* `vol` : Set volume level of a given NetStream object, from 0 to 1 (default 1).
* `pan` : Set left/right sound panning of a given NetStream object, from -1 to 1 (default 0). See Adobe's [official documentation](http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/media/SoundTransform.html#SoundTransform()) for details.

Options are optional unless specified.

### foresight_camera
    foresight_camera(opts:Object)
Manipulate camera objects. Returns a camera ID, or < 0 for error. Valid options are:

* `name` : Name of a camera on the computer. If not supplied the default is used.
* `stream` : Id of the stream to attach to for publishing.
* `width` : Width of the capture, default 160.
* `height` : Height of the capture, default 120.
* `bandwidth` : Bandwidth in bytes per second, default 0.
* `quality` : Quality, from 1 to 100, default 0.
* `fps` : Frame rate, default 15.
* `keyint` : Keyframe interval.
* `h264` : Parameters to enable the H.264 codec. Nested properties:
    * `profile`: H.264 [profile](http://en.wikipedia.org/wiki/H.264/MPEG-4_AVC#Profiles) to set. Valid values are `baseline` and `main`; default `baseline`.
    * `level`: H.264 [level](http://en.wikipedia.org/wiki/H.264/MPEG-4_AVC#Levels) to set. Must be a string. Valid values are 1, 1.2, 1.3, 1b, 2, 2.1, 2.2, 3, 3.1, 3.2, 4, 4.1, 4.2, 5, 5.1. Default 1.
* `close` : Id of the camera to close. Attached videos and/or streams must also be closed.

Options are optional unless specified. Unless the H.264 parameter is specified, FLV is used.

### foresight_mic
    foresight_mic(opts:Object)
Manipulate microphone objects. Returns a microphone ID, or < 0 for error. Valid options are:

* `name` : Name of a microphone on the computer. If not supplied the default is used.
* `stream` : Id of the stream to attach to for publishing.
* `gain` : The amount by which the microphpone boosts the signal.
* `rate` : Rate at which to capture sound, in kHz.
* `codec` : Codec to use. Available values are: `NellyMoser`, `Speex`, `pcma` and `pcmu`. Defaults to `NellyMoser`. For `Speex`, this option can be omitted if any of the following Speex options are set.
* `speex` : Options for the Speex speech codec.
    * `noiseSuppressionLevel` : int specifying noise suppression level.
    * `enableVAD` : Toggle voice activity detection with the value "true".
    * `encodeQuality` : Encoded speech quality.
    * `framesPerPacket` : Number of speech frames transmitted per message.
* `vol` : Set volume level of a given microphone object, from 0 to 1 (default 1).
* `pan` : Set left/right sound panning of a given microphone object, from -1 to 1 (default 0). See Adobe's [official documentation](http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/media/SoundTransform.html#SoundTransform()) for details.
* `loopback` : Plays audio on the selected microphone over local speakers.
* `silenceLevel` : Minimum audible level before an activity event is dispatched. Valid range is 0-100; recommended 0 with Speex.
* `silenceTimeout` : Length of silence, in milliseconds, before an activity event is dispatched. Defaults to 2000.
* `status` : Callback to dispatch status events for this microphone.
* `activity` : Callback to dispatch activity events for this microphone.
* `close` : Id of the microphone to close. Attached streams must also be closed.

### foresight_cameras
    foresight_cameras()
Returns an array of the cameras available to Flash.

### foresight_mics
    foresight_mics()
Returns an array of the microphones available to Flash.

### foresight_frame
    foresight_frame(id:int)
Returns a frame from the video specified by `id` as a PNG in base64 format, or null if the specified video is invalid.

### foresight_loglevel
    foresight_loglevel(level:int)

Sets the log level. Log entries are discarded if they are less than the loglevel. Current log entries are:

* 0 Fatal
* 1 Informative
* 2 Debug

The default log level is fatal. Set to -1 to ignore all log entries (including errors), or 99 to print all log entries.

### Example
    function handle_status(args) {
        var event = args[0];
        console.log("Received status"+event.info.code);
        if (event.code === "NetConnection.Connect.Success") {
            var streamid = foresight_play({"name": "beakybird", "callback": "handle_status"});
            foresight_video({"stream": streamid, "width": 160, "height": 120});
        } else if (event.code === "NetStream.Play.Start") {
            console.log("Playing stream "+args.details+" ("+args.target+")");
        }
    }
    function bw_done(args) {
        console.log("Received onBWDone");
    }
    foresight_ready() {
        foresight_loglevel(1); // informative log level
        foresight_new({"onStatus" : "handle_status", "onBWDone", "bw_done"})
        foresight_connect("rtmp://rtmp.account.foresightstreaming.com/account");
    }

## Notes
Calls from the swf to JS are buffered with at least 50ms in between calls to prevent being dropped by browsers.
